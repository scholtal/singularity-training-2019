# List of talks

## Josep
* What is a container? - 20min
* How to run services like RStudio or MongoDB - 30min (Hands-on)
* How to interact with the cluster - 20min (Hands-on)
* Installation and troubleshooting (Half Hands-on) - 20mins

## Michael
* Making containers - 90-120mins? (hard to tell as lots of hands-on)
* Remote container systems - 60mins
* Singularity and workflow management systems - 20mins


